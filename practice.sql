CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50) NOT NULL,
	address VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("ezrealAD", "ez", "Jarro Lightfeather", 2147483647, "ez@league.com", "Piltover");

CREATE TABLE reviews(
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	review VARCHAR(500) NOT NULL,
	datetime_created DATETIME NOT NULL,
	rating INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_review_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

INSERT INTO reviews (user_id, review, datetime_created, rating) VALUES (1 , "The songs are okay. Worth the subscription", "2023-05-03 00:00:00", 5);
INSERT INTO reviews (user_id, review, datetime_created, rating) VALUES (2 , "The songs are meh. I want BLACKPINK", "2023-01-23 00:00:00", 1);
INSERT INTO reviews (user_id, review, datetime_created, rating) VALUES (3 , "Add Bruno Mars and Lady Gaga", "2023-03-03 00:00:00", 4);
INSERT INTO reviews (user_id, review, datetime_created, rating) VALUES (4 , "I want to listen to more k-pop", "2022-09-23 00:00:00", 3);
INSERT INTO reviews (user_id, review, datetime_created, rating) VALUES (5, "Kindly add more OPM", "2023-02-01 00:00:00", 5);

SELECT * FROM users 
	JOIN reviews ON users.id = reviews.user_id
	WHERE full_name LIKE "%k%";

SELECT * FROM users 
	JOIN reviews ON users.id = reviews.user_id
	WHERE full_name LIKE "%x%";

SELECT * FROM reviews 
	JOIN users ON users.id = reviews.user_id;

SELECT review, username FROM users 
	JOIN reviews ON users.id = reviews.user_id;